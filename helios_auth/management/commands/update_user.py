"""
add a new user manually
"""

from django.core.management.base import BaseCommand, CommandError

from helios_auth.models import User
from helios_auth.auth_systems import AUTH_SYSTEMS


class Command(BaseCommand):
    args = ''
    help = 'update user'
    
    def add_arguments(self, parser):
        parser.add_argument('username', help='User id to update, usually an email address')
        parser.add_argument('-p', '--password', help='New password, does not make sense for all auth systems')
        parser.add_argument('-n', '--name', help='Display name, defaults to username')
        parser.add_argument('-t', '--token', help='New token value, does not make sense for all auth systems')
        parser.add_argument('-a', '--admin', type=int, choices=(0,1),
            help='Set admin boolean')
        parser.add_argument('-T', '--type', choices=AUTH_SYSTEMS.keys(),
            help='Type of user account')
    
    def handle(self, *args, **options):
#        self.stdout.write(repr((args, options)))
        u = User.objects.get(user_id=options['username'])
#        self.stdout.write(repr(u))
        info = u.info or {}
        u.info = info
        if options['name'] is not None:
            info['name'] = options['name']
        if options['password'] is not None:
            info['password'] = options['password']
        if options['type'] is not None:
            u.user_type = options['type']
        if options['token'] is not None:
            u.token = options['token']
        if options['admin'] is not None:
            u.admin_p = bool(options['admin'])
        u.save()
