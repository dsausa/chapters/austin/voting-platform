"""
add a new user manually
"""

from django.core.management.base import BaseCommand, CommandError

from helios_auth.models import User
from helios_auth.auth_systems import AUTH_SYSTEMS


class Command(BaseCommand):
    args = ''
    help = 'add users'
    
    def add_arguments(self, parser):
        parser.add_argument('username', help='User name to login with')
        parser.add_argument('-p', '--password', help='Set password, does not make sense for all auth types')
        parser.add_argument('-n', '--name', help='Display name, defaults to username')
        parser.add_argument('-t', '--token', help='New token value, does not make sense for all auth types (must be valid JSON)')
        parser.add_argument('-a', '--admin', default=False, action='store_true',
            help='Make an admin user')
        parser.add_argument('-T', '--type', default='password', choices=AUTH_SYSTEMS.keys(),
            help='Type of user account')
    
    def handle(self, *args, **options):
#        self.stdout.write(repr((args, options)))
        u = User.objects.create(user_type=options['type'], user_id=options['username'],
                                info={'name':options['name'] or options['username'],
                                      'password':options['password']},
                                token=options['token'],
                                admin_p=options['admin'])
#        self.stdout.write(repr(u))
